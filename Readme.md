Token Ring simulation.
The idea is that a handful of instances of the program are connected in a ring (i.e. a closed chain where every element sends messages to "next" and receives from "previous"). To send a message the instance must receive a token which is supposed to be released to "next" client in the ring right after delivery is confirmed.

At the moment the project works only when run in correct order and many things may go wrong so it still needs some work. Additionally it needs code cleaning and maybe refactoring. Also making the message sending asynchronous could be nice.

Build:
$cmake .
$make
