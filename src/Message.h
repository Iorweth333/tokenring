//
// Created by karolb on 11.03.19.
//

#ifndef TOKENRING_MESSAGE_H
#define TOKENRING_MESSAGE_H

#include <iostream>



using namespace std;

/* Message format:
 * source_address:source_listening_port,destination_address:destination_port,type,delivery_confirmation_flag,there_is_newcomer_flag,data
 * colons and commas are significant, while underscores are just part of names
 * Type and flags are singe characters, the rest is flexible
 */


class Message
{
public:
    enum msg_type{
        INIT = 0,
        ACK = 1,        //tis is for acknowledging of attaching new client, not for message delivery confiramtion
        MESSAGE = 2,
        RELEASE = 3
    };

    const string &getRawMsgStr() const;
    const string &getSourceAddress() const;
    const string &getSourcePort() const;
    const string &getDestinationAddress() const;
    const string &getDestinationPort() const;
    msg_type getType() const;
    bool getDeliveryConfirmationFlag() const;
    bool getThereIsNewcomerFlag() const;
    const string &getData() const;

    void setRawMsgStr(const string &raw_msg_str);
    void setSourceAddress(const string &source_address);
    void setSourcePort(const string &source_listening_port);
    void setDestinationAddress(const string &destination_address);
    void setDestinationPort(const string &destination_port);
    void setType(msg_type type);
    void setDeliveryConfirmationFlagTo(bool has_been_delivered);
    void setData(const string &data);
    void setThereIsNewcomer(bool val);

    explicit Message(string str);

    Message();

    string toString();

private:
    string raw_msg_str;

    string source_address;
    string source_listening_port;
    string destination_address;
    string destination_port;
    msg_type type;
    bool has_been_delivered;
    bool there_is_newcomer = false;
    string data;

    string getSourceAddress(string msg_str);
    string getSourceListeningPort(string msg_str);
    string getDestinationAddress (string msg_str);
    string getDestinationPort(string msg_str);
    string getMessageType (string msg_str);
    string getDeliveryConfirmationFlag(string msg_str);
    string getNewcomerFlag(string msg_str);
    string getData(string msg_str);
};



#endif //TOKENRING_MESSAGE_H
