//


#include "RingSetuper.h"


RingSetuper::RingSetuper(unsigned long listening_port) : listening_port(listening_port) {}


int RingSetuper::reachOutTo(const string address, unsigned long port_number) {
    address_to_reach_out_to.sin_port = htons(port_number);
    address_to_reach_out_to.sin_family = AF_INET;
    address_to_reach_out_to.sin_addr.s_addr = inet_addr(address.c_str());

    output_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    //the last argument, protocol, is irrelevant here- only one protocol exists

    if (output_socket_fd == 0) throw string("Reachout socket creation failed");
    int err = inet_pton(AF_INET, address.c_str(),
                        &address_to_reach_out_to.sin_addr.s_addr);    //convert address to binary form
    if (err != 1) throw string("inet_pton failed. code: " + to_string(err) + "\n");

    cout << "Trying to connect...\n";
    if (connect(output_socket_fd, (struct sockaddr *) &address_to_reach_out_to,
                sizeof(address_to_reach_out_to)) == -1)
        throw string("connect failed!");
    else cout << "Connected!\n";


    Message init_msg;
    init_msg.setSourceAddress(getMyAddress());
    init_msg.setSourcePort(to_string(
            listening_port));   //for establishing connection i must pass here listening port, not accepted (which is empty so far)
    init_msg.setDestinationAddress(address);
    init_msg.setDestinationPort(to_string(port_number));
    init_msg.setType(Message::INIT);
    init_msg.setDeliveryConfirmationFlagTo(false);
    init_msg.setThereIsNewcomer(true);
    init_msg.setData("--");
    //Delivery flag and data are irrelevant but I put them there for the sake of protocol


    string msg_str = init_msg.toString();
    cout << "Trying to send an init message: " << msg_str << "\n";
    unsigned long msg_str_len = msg_str.length();
    char *cstr = new char[msg_str_len + 1];
    strcpy(cstr, msg_str.c_str());

    err = send(this->output_socket_fd, cstr, msg_str_len + 1, 0);

    delete[] cstr;
    if (err == -1) throw string("Send failed!");
    else cout << "Message sent\n";
    return this->output_socket_fd;
}

int RingSetuper::reattach(string address, unsigned long port_number) {
    this->address_to_reach_out_to.sin_port = htons(port_number);
    this->address_to_reach_out_to.sin_family = AF_INET;
    this->address_to_reach_out_to.sin_addr.s_addr = inet_addr(address.c_str());     //

    this->output_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    //the last argument, protocol, is irrelevant here- only one protocol exists

    if (this->output_socket_fd == 0) throw string("Reachout socket creation failed");
    int err = inet_pton(AF_INET, address.c_str(),
                        &address_to_reach_out_to.sin_addr.s_addr);    //convert address to binary form
    if (err != 1) throw string("inet_pton failed. code: " + to_string(err) + "\n");

    cout << "Trying to connect.\n";
    if (connect(this->output_socket_fd, (struct sockaddr *) &this->address_to_reach_out_to,
                sizeof(this->address_to_reach_out_to)) == -1)
        throw string("connect failed!");
    else cout << "Connected!\n";


    Message init_msg;
    init_msg.setSourceAddress(this->getMyAddress());
    init_msg.setSourcePort(to_string(
            this->listening_port));
    init_msg.setDestinationAddress(address);
    init_msg.setDestinationPort(to_string(port_number));
    init_msg.setType(Message::ACK);
    init_msg.setDeliveryConfirmationFlagTo(false);
    init_msg.setData("---");
    //Delivery flag and data are irrelevant but I put them there for the sake of protocol


    string msg_str = init_msg.toString();
    cout << "Trying to send a message: " << msg_str << "\n";
    unsigned long msg_str_len = msg_str.length();
    char *cstr = new char[msg_str_len + 1];
    strcpy(cstr, msg_str.c_str());

    err = send(this->output_socket_fd, cstr, msg_str_len + 1, 0);

    delete[] cstr;
    if (err == -1) throw string("Send failed!");
    else cout << "Acknowledging message sent\n";
    return this->output_socket_fd;
}

string RingSetuper::getMyAddress() {
    char buf[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &this->listen_address_info.sin_addr, buf,
              INET_ADDRSTRLEN);   //convert address from binary to text
    if (strcmp("0.0.0.0", buf) == 0)
        strcpy(buf, "127.0.0.1");
    return string(buf);
}

//returns file descriptor of accepted socket
int RingSetuper::acceptConnection() {
    memset(&listen_address_info, '\0', sizeof(listen_address_info));

    listen_address_info.sin_port = htons(listening_port);
    listen_address_info.sin_family = AF_INET;
    listen_address_info.sin_addr.s_addr = inet_addr("127.0.0.1");     //localhost
    int flag = 1;    //optval
    //debug
    //debug
//    int port= 4000;
//
//    for (int i=0; i<1000; i++){
//        listening_socket_fd = socket(AF_INET, SOCK_STREAM, 0);           //| SOCK_NONBLOCK, for nonblocking
//        if (listening_socket_fd == 0) throw string("Listening socket creation failed");
//        if (setsockopt(listening_socket_fd, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(int)) == -1)
//            throw string("Receiver setsockopt failed!");   //assuring port reuse
//        int res = bind(listening_socket_fd, (struct sockaddr *) &listen_address_info, sizeof(listen_address_info));
//        cout<<"port: "<<port<<" socket: "<<listening_socket_fd<<" res: "<< res<<endl;
//        port++;
//    }
//

    listening_socket_fd = socket(AF_INET, SOCK_STREAM, 0);           //| SOCK_NONBLOCK, for nonblocking

    if (listening_socket_fd == 0) throw string("Listening socket creation failed");

    if (setsockopt(listening_socket_fd, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(int)) == -1)
        throw string("Receiver setsockopt failed!");   //assuring port reuse

    if (bind(listening_socket_fd, (struct sockaddr *) &listen_address_info, sizeof(listen_address_info)) == -1)
        throw string("Receiver bind failed");
    else cout << "binding successfull\n";

    char buf[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &this->listen_address_info.sin_addr, buf,
              INET_ADDRSTRLEN);   //convert address from binary to text
    cout << "Your address is: " << buf << " and you listen at port:" << ntohs(listen_address_info.sin_port)
         << "\n";

    if (listen(this->listening_socket_fd, this->max_incoming_queue_len) == -1) throw string("Listening failed");


    this->accepted_connection_addr_info_size = sizeof(accepted_address_info);

    input_socket_fd = accept(this->listening_socket_fd, (struct sockaddr *) &this->accepted_address_info,
                             (socklen_t *) &this->accepted_connection_addr_info_size);

    char acc_addr_buf[INET_ADDRSTRLEN];
    //convert address from binary to text
    inet_ntop(AF_INET, &accepted_address_info.sin_addr, acc_addr_buf, INET_ADDRSTRLEN);
    if (input_socket_fd == -1) throw string("Accepting failed");
    else
        cout << "Connection accepted. Accepted address: " << acc_addr_buf << " "
             << ntohs(accepted_address_info.sin_port) << "\n";
    return input_socket_fd;
}

int RingSetuper::checkIfThereIsNewcomer() {
    struct sockaddr_in newcomer_address_info;
    int newcomer_address_info_size = sizeof(newcomer_address_info);

    int newcomer_socket_fd = accept(listening_socket_fd, (struct sockaddr *) &newcomer_address_info,
                                    (socklen_t *) &newcomer_address_info_size);


    if (newcomer_socket_fd == -1) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) return 0;
        else throw string("Accepting failed while checking out for newcomers");
    }
    else {
        this->input_socket_fd = newcomer_socket_fd;
        this->accepted_address_info = newcomer_address_info;
        char acc_addr_buf[INET_ADDRSTRLEN];
        //convert address from binary to text
        inet_ntop(AF_INET, &accepted_address_info.sin_addr, acc_addr_buf, INET_ADDRSTRLEN);
        cout << "Connection accepted. Accepted address: " << acc_addr_buf << " "
             << ntohs(accepted_address_info.sin_port) << "\n";
        return newcomer_socket_fd;
    }
}

int RingSetuper::getMyListeningPort() {
    return ntohs(this->listen_address_info.sin_port);
}

//listening port and input ports are the same
int RingSetuper::getMyInputPort() {
    return ntohs(this->listen_address_info.sin_port);
}

unsigned int
RingSetuper::getInputPortOfMyNeighbour() {              //port of my neighbour (the next person in the ring)
    return ntohs(this->address_to_reach_out_to.sin_port);
}

string RingSetuper::getAddressOfMyNeighbour() {
    char buf[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &this->address_to_reach_out_to.sin_addr, buf,
              INET_ADDRSTRLEN);   //convert address from binary to text
    if (strcmp("0.0.0.0", buf) == 0)
        strcpy(buf, "127.0.0.1");
    return string(buf);
}

string RingSetuper::getAddressOfMyPrevious() {
    char buf[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &this->accepted_address_info.sin_addr, buf,
              INET_ADDRSTRLEN);   //convert address from binary to text
    if (strcmp("0.0.0.0", buf) == 0)
        strcpy(buf, "127.0.0.1");
    return string(buf);
}

bool RingSetuper::isTheMessageToMe(Message msg) {
    return (msg.getDestinationAddress() == this->getMyAddress() &&
            msg.getDestinationPort() == to_string(this->getMyInputPort()));
}

bool RingSetuper::isTheMessageFromMe(Message msg) {
    return (msg.getSourceAddress() == this->getMyAddress() &&
            msg.getSourcePort() == to_string(this->getMyInputPort()));
    //IMPORTANT! SourcePort is not exactly port that the message was sent through.
    //Technically it's "Input port of the one who is the source of the message"
    //It's also something that identifies him
}

bool RingSetuper::isTheMessageToMyNeighbour(Message msg) {
    return (msg.getDestinationAddress() == this->getAddressOfMyNeighbour()
            && msg.getDestinationPort() == to_string(getInputPortOfMyNeighbour()));
}

void RingSetuper::setListeningSocketToNonblock() {
    //int option = 1;
    //if (setsockopt(this->listening_socket_fd, SOL_SOCKET, SOCK_NONBLOCK, (char *) &option, sizeof(int)) == -1)
    //    throw string("Setsockopt failed while setting to Nonblock!");   //assuring port reuse
    if (fcntl(this->listening_socket_fd, F_SETFL, O_NONBLOCK) == -1) throw string("Failed while setting to Nonblock!");;
}

RingSetuper::~RingSetuper() {
    shutdown(this->input_socket_fd, SHUT_RDWR);
    shutdown(this->output_socket_fd, SHUT_RDWR);
}

void RingSetuper::cleanup() {
    shutdown(this->input_socket_fd, SHUT_RDWR);
    shutdown(this->output_socket_fd, SHUT_RDWR);
}

bool RingSetuper::isTheMessageFromMyNeighbour(Message msg) {
    return (msg.getSourceAddress() == getAddressOfMyNeighbour() &&
            msg.getSourcePort() == to_string(getInputPortOfMyNeighbour()));
}





