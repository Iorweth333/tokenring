//
// Created by karolb on 11.03.19.
//

#ifndef TOKENRING_TCPSENDER_H
#define TOKENRING_TCPSENDER_H

#include <sys/socket.h>
#include <iostream>
#include <netinet/in.h>
#include "Message.h"



using namespace std;

class TCPSender
{
private:
    int output_socket_fd;
    struct sockaddr_in address_info;
    bool i_have_token;
public:
    TCPSender();

    explicit TCPSender(int output_socket_fd, bool token);

    void sendMessage(Message msg);
    void releaseToken(Message msg_to_be_complemented);
    void tokenReceived();

    bool iHaveToken();

    void setOutput_socket_fd(int output_socket_fd);
    bool closeCurrentConnection();
};


#endif //TOKENRING_TCPSENDER_H
