//
// Created by karolb on 11.03.19.
//

#include "Message.h"
#include<cstdlib>

//sourceAddr:port,destAddr:port,type,deliverConf,newcomerFlag,data

Message::Message(const string str)
{
    this->raw_msg_str = str;
    this->source_address = getSourceAddress(str);
    this->source_listening_port = getSourceListeningPort(str);
    this->destination_address = getDestinationAddress(str);
    this->destination_port = getDestinationPort(str);
    long m_type = strtol (getMessageType(str).c_str(), nullptr, 10);
    switch (m_type){
        case INIT:
            this->type = msg_type(INIT);
            break;
        case ACK:
            this->type = msg_type(ACK);
            break;
        case MESSAGE:
            this->type = msg_type(MESSAGE);
            break;
        case RELEASE:
            this->type = msg_type(RELEASE);
            break;
        default:
            throw string("Unknown message type: " + to_string(m_type) + ".");
            //this whole switch-case is for the sake of handling exception and converting long to enum
    }
    this->has_been_delivered = (getDeliveryConfirmationFlag(str) == "1");
    this->there_is_newcomer = (getNewcomerFlag(str) == "1");
    this->data = getData(str);
}

Message::Message()
= default;

string Message::getSourceAddress(const string msg_str)
{
    return msg_str.substr(0, msg_str.find(':'));
}

string Message::getSourceListeningPort(string msg_str)
{
    unsigned long colon_pos = msg_str.find(':');
    unsigned long comma_pos = msg_str.find(',');
    unsigned long port_number_len = comma_pos - colon_pos - 1;
    return msg_str.substr(colon_pos + 1, port_number_len);
}

string Message::getDestinationAddress(const string msg_str)
{
    unsigned long first_comma_pos = msg_str.find(',');
    unsigned long second_colon_pos = msg_str.find(':', first_comma_pos);
    unsigned long address_len = second_colon_pos - first_comma_pos - 1;
    return msg_str.substr(first_comma_pos + 1, address_len);
}

string Message::getDestinationPort(const string msg_str)
{
    unsigned long first_comma_pos = msg_str.find(',');
    unsigned long second_comma_pos = msg_str.find(',', first_comma_pos + 1);
    unsigned long second_colon_pos = msg_str.find(':', first_comma_pos);
    unsigned long port_number_len = second_comma_pos - second_colon_pos - 1;
    return msg_str.substr(second_colon_pos + 1, port_number_len);
}

string Message::getMessageType(const string msg_str)
{
    unsigned long first_comma_pos = msg_str.find(',');
    unsigned long second_comma_pos = msg_str.find(',', first_comma_pos + 1);
    return msg_str.substr(second_comma_pos + 1, 1);
}

string Message::getDeliveryConfirmationFlag(const string msg_str)
{
    unsigned long first_comma_pos = msg_str.find(',');
    unsigned long second_comma_pos = msg_str.find(',', first_comma_pos + 1);
    return msg_str.substr(second_comma_pos + 3, 1);
}

string Message::getNewcomerFlag(string msg_str) {
    unsigned long first_comma_pos = msg_str.find(',');
    unsigned long second_comma_pos = msg_str.find(',', first_comma_pos + 1);
    return msg_str.substr(second_comma_pos + 5, 1);
}

string Message::getData(const string msg_str)
{
    unsigned long first_comma_pos = msg_str.find(',');
    unsigned long second_comma_pos = msg_str.find(',', first_comma_pos + 1);
    return msg_str.substr(second_comma_pos + 7);
}

const string &Message::getRawMsgStr() const
{
    return raw_msg_str;
}

const string &Message::getSourceAddress() const
{
    return source_address;
}

const string &Message::getSourcePort() const
{
    return source_listening_port;
}

const string &Message::getDestinationAddress() const
{
    return destination_address;
}

const string &Message::getDestinationPort() const
{
    return destination_port;
}

Message::msg_type Message::getType() const
{
    return type;
}

bool Message::getDeliveryConfirmationFlag() const
{
    return has_been_delivered;
}

const string &Message::getData() const
{
    return data;
}

void Message::setRawMsgStr(const string &raw_msg_str)
{
    Message::raw_msg_str = raw_msg_str;
}

void Message::setSourceAddress(const string &source_address)
{
    Message::source_address = source_address;
}

void Message::setSourcePort(const string &source_listening_port)
{
    Message::source_listening_port = source_listening_port;
}

void Message::setDestinationAddress(const string &destination_address)
{
    Message::destination_address = destination_address;
}

void Message::setDestinationPort(const string &destination_port)
{
    Message::destination_port = destination_port;
}

void Message::setType(Message::msg_type type)
{
    Message::type = type;
}

void Message::setDeliveryConfirmationFlagTo(bool has_been_delivered)
{
    Message::has_been_delivered = has_been_delivered;
}

void Message::setData(const string &data)
{
    Message::data = data;
}

string Message::toString()
{
    return source_address + ":" + source_listening_port + "," + destination_address + ":" + destination_port + "," +
           to_string(type) + "," + to_string(has_been_delivered) + "," + to_string(there_is_newcomer) + "," + data;
}

void Message::setThereIsNewcomer(bool val) {
    this->there_is_newcomer = val;
}

bool Message::getThereIsNewcomerFlag() const {
    return there_is_newcomer;
}


