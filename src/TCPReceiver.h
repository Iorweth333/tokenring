//
// Created by karolb on 11.03.19.
//

#ifndef TOKENRING_RECEIVER_H
#define TOKENRING_RECEIVER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include "Message.h"
#include <fcntl.h>

#define data_buffer_size 1024


using namespace std;

class TCPReceiver
{
private:
    int input_socket_fd;
    char buffer[data_buffer_size];
public:
    explicit TCPReceiver(int accepted_socket_fd);

    TCPReceiver();

    Message receive();

    int closeCurrentConnection();

    void setInput_socket_fd(int input_socket_fd);

    void setInputSocketToNonblocking();

};


#endif //TOKENRING_RECEIVER_H
