//
// Created by karolb on 11.03.19.
//

#include "TCPReceiver.h"
#include <sys/socket.h>
#include <iostream>
#include <arpa/inet.h>

#include <sys/stat.h>   //debug
#include <unistd.h>

using namespace std;

TCPReceiver::TCPReceiver(int accepted_socket_fd)
{
    this->input_socket_fd = accepted_socket_fd;
}

Message TCPReceiver::receive()
{
    cout << "Waiting for messages; socket " <<input_socket_fd<<"\n";
    long size =  recv(this->input_socket_fd, this->buffer, data_buffer_size, 0);    //TODO: this-> buffer or &this->buffer ??
    if(size == -1) throw string ("Error while receiving message.");
    if (size == 0) throw string("Connection closed.");
    string raw_msg_str(buffer);
    return Message (raw_msg_str);
}

void TCPReceiver::setInput_socket_fd(int input_socket_fd) {
    this->input_socket_fd = input_socket_fd;
}

void TCPReceiver::setInputSocketToNonblocking() {
    if (fcntl (this->input_socket_fd, F_SETFL, O_NONBLOCK ) == -1 ) throw string("Failed while setting to Nonblock!"); ;
    struct timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    if (setsockopt (this->input_socket_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                    sizeof(timeout)) < 0)
        throw string("setsockopt failed while setting timeout\n");
}

int TCPReceiver::closeCurrentConnection() {
    return close(input_socket_fd);
}

TCPReceiver::TCPReceiver() = default;



