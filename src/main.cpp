#include <iostream>
#include <cstring>
#include "TCPReceiver.h"
#include "TCPSender.h"
#include "RingSetuper.h"
#include <thread>
#include <mutex>

mutex token_mutex;
mutex fd_mutex;

using namespace std;

struct init_data {
    string newcomer__id;
    int newcomer_listening_port;
    string IP_newcomer_sends_to;
    int port_newcomer_sends_to;
};

void incomingMessagesHandler(TCPReceiver &receiver, TCPSender &sender, RingSetuper &setuper);

void handleNewConnection(RingSetuper &setuper, TCPReceiver &receiver, TCPSender &sender);

int main(int argc, char *argv[]) {
/*
    tekstowy identyfikator użytkownika,
            port na którym dany klient nasłuchuje,
            adres IP i port sąsiada, do którego przekazywane będą wiadomości,
            informacja o tym, czy dany użytkownik po uruchomieniu posiada token,
            wybrany protokół: tcp lub udp.

*/



    //interpreting command line arguments
    //neighbour == next person in the ring
    if (argc != 7) {     //remember about path in argv[0]
        cout << "Wrong number of parameters (need 6):\n"
                "User ID\n"
                "listening port\n"
                "IP address and port number of the neighbour who will receive messages\n"
                "flag indicating if this user has a token\n"
                "chosen protocol: TCP or UDP\n";
        return 1;
    }
    string userID = argv[1];
    unsigned long my_listening_port = strtoul(argv[2], nullptr, 0);
    if (my_listening_port == 0) {
        cout << "Unable to convert listening port number. The program will close.\n";
        return 1;
    }
    string neighbour_IP = argv[3];
    unsigned long neighbour_port = strtoul(argv[4], nullptr, 0);
    if (neighbour_port == 0) {
        cout << "Unable to convert neighbour port number. The program will close.\n";
        return 1;
    }
    bool im_alone = false;
    if ((neighbour_IP == "127.0.0.1" || neighbour_IP == "0.0.0.0") && neighbour_port == 1) im_alone = true;
    bool do_i_have_token = false;
    if (strcmp(argv[5], "yes") == 0 || strcmp(argv[5], "y") == 0 || strcmp(argv[5], "1") == 0 ||
        strcmp(argv[5], "true") == 0 || strcmp(argv[5], "t") == 0)
        do_i_have_token = true;
    string protocol = argv[6];
    if (protocol != "tcp" && protocol != "udp") {
        cout << "Unknown protocol. The program will close\n";
        return 1;
    }




    //starting the work
    if (protocol == "tcp") {
        try {
            RingSetuper setuper(my_listening_port);
            int input_socket_fd;
            int output_socket_fd;
            TCPReceiver receiver;
            if (im_alone) {
                cout << "You are alone so far.\n";
                input_socket_fd = setuper.acceptConnection();       //wait for other user to join the ring
                receiver.setInput_socket_fd(input_socket_fd);
                Message msg = receiver.receive();
                output_socket_fd = setuper.reachOutTo(setuper.getAddressOfMyNeighbour(),
                                                      strtoul(msg.getSourcePort().c_str(), nullptr, 0));

            }
            else {
                output_socket_fd = setuper.reachOutTo(neighbour_IP, neighbour_port);  //we ask to be included
                cout << "Waiting for being connected\n";
                input_socket_fd = setuper.acceptConnection();                //we accept connection from the one who will be "before" us
                receiver.setInput_socket_fd(input_socket_fd);
            }

            TCPSender sender(output_socket_fd, do_i_have_token);

            setuper.setListeningSocketToNonblock();
            //receiver.setInputSocketToNonblocking();   //TODO uncomment this if useful again

            //At this point we should be a full member of a working ring

            //TODO: uncomment this if it's useful again
            //thread asynchronous_receiving(incomingMessagesHandler, ref(receiver), ref(sender), ref(setuper));

            while (true) {
                handleNewConnection(setuper, receiver, sender);

                //TODO: CHECK IF IT'S MESSAGE I'VE SENT (FOR THE SAKE OF CIRCULATING MESSAGES IF SOMEBODY MADE A TYPO IN ADDRESS)

                //receiving
                fd_mutex.lock();
                Message msg = receiver.receive();
                fd_mutex.unlock();

                cout << "Receiver returned: <" << msg.toString() << ">" << endl;

                //there_is_newcomer = msg.getThereIsNewcomerFlag();

                bool is_the_message_to_my_neighbour = setuper.isTheMessageToMyNeighbour(msg);
                bool is_the_message_from_me = setuper.isTheMessageFromMe(msg);
                bool is_the_message_to_me = setuper.isTheMessageToMe(msg);

                if (is_the_message_to_my_neighbour &&
                    !is_the_message_from_me &&
                    msg.getThereIsNewcomerFlag()) {          //newcomer comes between me and the one i send to
                    cout << "There is newcomer you send now to\n";
                    int new_socket_fd = setuper.reattach(msg.getSourceAddress(),
                                                         strtoul(msg.getSourcePort().c_str(), nullptr, 10));
                    if (sender.closeCurrentConnection() != 0)
                        cout << "sender closing current connection failed. Errno " << errno << strerror(errno)
                             << endl;
                    sender.setOutput_socket_fd(new_socket_fd);

                }
                else {
                    if (is_the_message_to_me) {
                        switch (msg.getType()) {
                            case Message::INIT: {       //newcomer comes between me and the one I received messages from
                                //receiver = TCPReceiver()
                                //Receiving socket is changed in while loop after accepting new connection
                                cout << "Passing init along\n";
                                msg.setSourceAddress(setuper.getAddressOfMyNeighbour());
                                msg.setThereIsNewcomer(true);
                                msg.setDeliveryConfirmationFlagTo(true);
                                //need to change source address for newcomer's address because in the message there is always "0.0.0.0"- newcomer's listening socket isn't bound yet
                                sender.sendMessage(msg);    //pass along
                                break;
                            }
                            case Message::MESSAGE: {
                                cout << "Message received: " << msg.getData() << endl;
                                msg.setDeliveryConfirmationFlagTo(true);
                                sender.sendMessage(
                                        msg);        //to assure author of the message that it's has been delivered
                                break;
                            }
                            case Message::RELEASE: {
                                token_mutex.lock();
                                sender.tokenReceived();
                                token_mutex.unlock();
                                cout << "Token received!" << endl;
                                break;
                            }
                            case Message::ACK:
                                cout << "You have been connected to the ring\n";
                                break;
                            default:
                                //throw string("Unknown message type: " + to_string(msg.getType()) + ".\n");
                                cout << "Unknown message type: " + to_string(msg.getType()) + ".\n";
                        }
                    }
                    else {
                        if (is_the_message_from_me && !msg.getDeliveryConfirmationFlag()) {
                            if (msg.getDeliveryConfirmationFlag()) {
                                cout << "Your message has been confirmed\n";
                            }
                            else {
                                cout << "Your message has NOT been confirmed!\n";
                            }
                            cout << "releasing token\n";
                            Message msg_to_be_complemented;
                            msg_to_be_complemented.setDestinationAddress(setuper.getAddressOfMyNeighbour());
                            msg_to_be_complemented.setDestinationPort(
                                    to_string(setuper.getInputPortOfMyNeighbour()));
                            token_mutex.lock();
                            sender.releaseToken(msg_to_be_complemented);
                            token_mutex.unlock();
                        }
                        else {
                            cout << "Passing along a message\n";
                            sender.sendMessage(msg);    //in any other cases I just pass the message along

                        }
                    }
                }





                //sending
                if (sender.iHaveToken()) {

                    cout << "You have a token! ";
                    cout << "Provide destination address or type /skip:\n";
                    string dest_address;
                    cin >> dest_address;
                    if (dest_address == "/exit") break;
                    Message msg_to_be_complemented;
                    msg_to_be_complemented.setDestinationAddress(setuper.getAddressOfMyNeighbour());
                    msg_to_be_complemented.setDestinationPort(to_string(setuper.getInputPortOfMyNeighbour()));
                    if (dest_address == "/skip") {
                        handleNewConnection(setuper, receiver, sender);
                        token_mutex.lock();
                        sender.releaseToken(msg_to_be_complemented);
                        token_mutex.unlock();
                    }
                    else {
                        cout << "Provide port number: \n";
                        unsigned long dest_port;
                        cin >> dest_port;
                        cout << "Provide data: \n";
                        string data;
                        cin >> data;
                        Message msg_to_be_sent;

                        msg_to_be_sent.setDeliveryConfirmationFlagTo(false);
                        msg_to_be_sent.setType(Message::MESSAGE);
                        msg_to_be_sent.setData(data);
                        msg_to_be_sent.setDestinationPort(to_string(dest_port));
                        msg_to_be_sent.setDestinationAddress(dest_address);
                        msg_to_be_sent.setSourceAddress(setuper.getMyAddress());
                        msg_to_be_sent.setSourcePort(to_string(setuper.getMyInputPort()));
                        handleNewConnection(setuper, receiver, sender);
                        token_mutex.lock();
                        sender.sendMessage(msg_to_be_sent);
                        token_mutex.unlock();
                    }

                }


            }


        }
        catch (string &s) {
            cout << s << "\n" << "Errno " << errno << ": " << strerror(errno) << "\n";
        }
        catch (...) {
            cout << "Default exception occurred in main thread!\n";
        }

    }
    if (protocol == "udp") {
        cout << "Not implemented yet.\n";
    }


    cout << "Closing\n";
    return 0;
}

void handleNewConnection(RingSetuper &setuper, TCPReceiver &receiver, TCPSender &sender) {
    bool there_is_newcomer = false;
    //checking for newcomers to the ring
    cout << "Checking out for newcomers. ";
    int newcomer_socket_fd = 0;
    newcomer_socket_fd = setuper.checkIfThereIsNewcomer();


    if (newcomer_socket_fd != 0) {
        cout << "There is a newcomer!\n";
        //fd_mutex.lock();
        receiver.closeCurrentConnection();
        receiver.setInput_socket_fd(newcomer_socket_fd);
        there_is_newcomer = true;
        //receiver.setInputSocketToNonblocking();           //TODO: uncomment these if useful again
        //fd_mutex.unlock();

        //receiving init
        cout << "Waiting for init message " << endl;
        fd_mutex.lock();
        Message msg = receiver.receive();
        fd_mutex.unlock();

        cout << "Receiver returned: <" << msg.toString() << ">" << endl;

        msg.setSourceAddress(setuper.getAddressOfMyPrevious()); //the rest should be set already
        msg.setThereIsNewcomer(true);
        cout<<"Passing newcomer's init\n";
        sender.sendMessage(msg);

    }
    else cout << "No newcomers found\n";



}


//deprecated
void incomingMessagesHandler(TCPReceiver &receiver, TCPSender &sender, RingSetuper &setuper) {
    cout << "starting async reception\n";
    try {
        cout << "'";
        while (true) {

            token_mutex.lock();
            if (sender.iHaveToken()) {}

            else
                cout << "You don't have token! Message not sent\n";
            token_mutex.unlock();
        }


    }

    catch (string &s) {
        cout << s << "\n" << "Errno " << errno << ": " << strerror(errno) << "\n";
    }
    catch (...) {
        cout << "Default exception occurred!\n";
    }

}
