//
// Created by karolb on 15.03.19.
//

#ifndef TOKENRING_INIT_H
#define TOKENRING_INIT_H

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <fcntl.h>

#include "Message.h"

#define data_buffer_size 1024

using namespace std;

class RingSetuper {
private:
    int output_socket_fd;
    int listening_socket_fd;
    int input_socket_fd;
    struct sockaddr_in address_to_reach_out_to;
    int listening_port;
    struct sockaddr_in listen_address_info;
    struct sockaddr_in accepted_address_info;   //from this address messages are sent to me
    int accepted_connection_addr_info_size;

    int max_incoming_queue_len = 5;        //TODO: how much should I put here?

public:
    explicit RingSetuper(unsigned long listening_port);

    int reachOutTo(string address, unsigned long port_number);
    int reattach (string address, unsigned long port_number);
    int acceptConnection();                   //returns file descriptor of accepted socket
    int checkIfThereIsNewcomer();

    string getMyAddress();
    int getMyListeningPort();
    int getMyInputPort();
    unsigned int getInputPortOfMyNeighbour();    //port of my neighbour (the next person in the ring)
    string getAddressOfMyNeighbour();   //let's ignore for a while that it's always 127.0.0.1
    string getAddressOfMyPrevious();

    void setListeningSocketToNonblock();

    bool isTheMessageToMe(Message msg);
    bool isTheMessageFromMe(Message msg);
    bool isTheMessageToMyNeighbour (Message msg);
    bool isTheMessageFromMyNeighbour(Message msg);

    ~RingSetuper();
    void cleanup();
};


#endif //TOKENRING_INIT_H
