//
// Created by karolb on 11.03.19.
//

#include "TCPSender.h"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

using namespace std;

TCPSender::TCPSender(int output_socket_fd, bool token)
{
    this->output_socket_fd = output_socket_fd;
    this->i_have_token = token;
}



void TCPSender::sendMessage(Message msg)
{
    string msg_str = msg.toString();
    cout<<"Trying to send a message: "<<msg_str<<" socket: "<<output_socket_fd<<"\n";
    //debug
//    cout<<"Send?";
//    string i;
//    cin>>i;

    unsigned long msg_str_len = msg_str.length();
    char * cstr = new char [msg_str_len+1];
    strcpy (cstr, msg_str.c_str());
    if (send(this->output_socket_fd, cstr, msg_str_len+1, 0) == -1) throw string ("Send failed!");
    else cout<<"Message sent\n";
}

void TCPSender::releaseToken(Message msg_to_be_complemented) {
    if (i_have_token){
        msg_to_be_complemented.setDeliveryConfirmationFlagTo(false);
        msg_to_be_complemented.setData("---");
        msg_to_be_complemented.setType(Message::RELEASE);
        msg_to_be_complemented.setSourceAddress(" ");
        msg_to_be_complemented.setSourcePort(" ");      //source is irrelevant
        this->sendMessage(msg_to_be_complemented);
        this->i_have_token = false;
    }
}

//TODO there should also go an info about receiving token on a multicast broadcast for the sake of loggers
void TCPSender::tokenReceived() {
    this->i_have_token = true;
}

void TCPSender::setOutput_socket_fd(int output_socket_fd) {
    TCPSender::output_socket_fd = output_socket_fd;
}

bool TCPSender::iHaveToken() {
    return i_have_token;
}

bool TCPSender::closeCurrentConnection() {
    return close(output_socket_fd);
}

TCPSender::TCPSender() = default;

